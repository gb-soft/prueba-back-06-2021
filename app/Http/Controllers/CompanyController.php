<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\Company\UpdateStatusCompanyRequest;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use App\Models\Company;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
	
	/**
     * Display all companies.
     *
     * @return \Illuminate\Http\Response
     */
    public function showList()
    {
       return Company::all();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try
		{
			$status = $request->input("action")=='Desactivar' ? 'inactive' : 'active';
        	DB::table('companies')->where('id', $request->input("id"))->update(['status' => $status]);
		}
		catch(Exception $ex)
		{
			response()->json(['error' => $ex->getMessage()]);
		}
		return response()->json(['success' => "ok"], 200);
    }
	
	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function apiUpdateStatus(UpdateStatusCompanyRequest $request, $id)
    {
		try
		{
        	DB::table('companies')->where('id', $id)->update(['status' => $request->input("status")]);
		}
		catch(Exception $ex)
		{
			response()->json(['error' => $ex->getMessage()]);
		}
		return response()->json(['success' => "ok"], 200);
		
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
