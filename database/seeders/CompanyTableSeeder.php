<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Company;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Company::truncate();

        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 3; $i++) {
            Company::create([
				/*'id' => is_null(Company::inRandomOrder()->first())?1:Company::inRandomOrder()->first()->id,*/
                'name' => $faker->sentence,
                'email' => $faker->sentence,
				'address' => $faker->sentence,
				'status' => 'active',
				
            ]);
        }
    }
}
