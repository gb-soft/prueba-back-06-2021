<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="description" content="Prueba para bewor" />
    <meta name="author" content="Gerardo Baltanás" />

    <title>Pruebas Bewor</title>
	
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    
    <link href = {{ asset("/css/app.css") }} rel="stylesheet" />
	<script src= {{ asset("/js/app.js") }} ></script>    
</head>
<body>
<?php

use App\Http\Controllers\CompanyController;

$c = new CompanyController();
$companies = $c->showList();

?>

<table id="categories_table" class="table table-striped table-bordered" style="width:90%; margin: 50px auto;">
    <thead>
        <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>E-mail</th>
            <th>Address</th>
            <th style="text-align:center">Status</th>
            <th style="text-align:center">Acciones</th>
        </tr>
    </thead>
    <tbody>
            

<?php
foreach($companies as $company)
{
	
		echo " <tr>
                <td>".$company->id."</td>
                <td>".$company->name."</td>
                <td>".$company->email."</td>
                <td>".$company->address."</td>
                <td style='text-align:center'>".($company->status == 'active' ? '<span class="fa fa-check" style="color:Green"></span>':'<span class="fa fa-times" style="color:FireBrick"></span>')."</td>
				<td style='text-align:center'>
					<div class='btn-group'>";
						if ($company->status == 'active')
							echo "<button name='btnChangeStatus'  title='Desactivar' class='btn btn-xs btn-danger' data-id='".$company->id."'><span class='fa fa-thumbs-down'></span>";
						else
							echo "<button name='btnChangeStatus'  title='Activar' class='btn btn-xs btn-success' data-id='".$company->id."'><i class='fa fa-thumbs-up'></i>";
					echo "</div>
				</td>
            </tr>";
            
}

?>

</tbody>
</table>

<script>
$(document).ready(function(e) {
   
   $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
   
   $("button[name='btnChangeStatus']").click(function()
	{
		element = $(this);
		action = element.attr("title");
		id = element.data("id");
		
		
		$.ajax({
			type: 'POST',
			url: "{{ route('CompanyController.update') }}",
			data: {action:action,id:id},
			success:function(data)
			{
				if(action == "Desactivar")
				{
					element.attr("title","Activar").removeClass("btn-danger").addClass("btn-success").html('<span class="fa fa-thumbs-up"></span>');
					element.parent().parent().prev().html('<span class="fa fa-times" style="color:FireBrick"></span>');
				}
				else if(action == "Activar")
				{
					element.attr("title","Desactivar").removeClass("btn-success").addClass("btn-danger").html('<span class="fa fa-thumbs-down"></span>');
					element.parent().parent().prev().html('<span class="fa fa-check" style="color:Green"></span>');
				}
			},
			error:function(err)
			{
				console.log("error");
				console.log(err);
			}
		});
		
		
		
		
	});
    
});

</script>

</body>
</html>